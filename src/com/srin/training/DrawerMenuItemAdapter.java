package com.srin.training;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class DrawerMenuItemAdapter extends BaseExpandableListAdapter {
	
	private Context context;
	private List<DrawerMenuItem> drawerMenuItems;
	
	public DrawerMenuItemAdapter(Context context, List<DrawerMenuItem> drawerMenuItems) {

		this.context = context;
		this.drawerMenuItems = drawerMenuItems;
	}

	@Override
	public int getGroupCount() {

		return drawerMenuItems.size();
	}
	
	@Override
	public Object getGroup(int groupPosition) {

		return drawerMenuItems.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {

		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		ViewHolder holder;
		
		if (convertView == null) {
			
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.text_list_item, null);
			
			holder = new ViewHolder();
			holder.textView = (TextView) convertView.findViewById(R.id.text_view);
			holder.textView.setTypeface(null, Typeface.BOLD);
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textView.setText(drawerMenuItems.get(groupPosition).getGroupItem().getName());
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		return drawerMenuItems.get(groupPosition).getChildItem().size();
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {

		return drawerMenuItems.get(groupPosition).getChildItem().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {

		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		ViewHolder holder;
		
		if (convertView == null) {
			
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.text_list_item, null);
			
			holder = new ViewHolder();
			holder.textView = (TextView) convertView.findViewById(R.id.text_view);
			convertView.setTag(holder);
			
		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textView.setText(drawerMenuItems.get(groupPosition).getChildItem().get(childPosition).getName());
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {

		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {

		return true;
	}

	static class ViewHolder {
		
		TextView textView;
	}
}
