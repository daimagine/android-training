package com.srin.training.day5.service;

import java.io.File;
import java.io.IOException;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.srin.training.R;
import com.srin.training.day5.service.FileObserverService.FileObserverBinder;
import com.srin.training.day5.service.FileObserverService.NotificationListener;

public class BoundServiceExampleFragment extends Fragment implements NotificationListener {
	
	private Button button;
	private TextView textView;
	
	private FileObserverService fileObserverService;
	
	private ServiceConnection conn = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			
			if (fileObserverService != null) {
				
				fileObserverService.unregisterNotificationListener(BoundServiceExampleFragment.this);
				fileObserverService = null;
			}
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			
			if (service instanceof FileObserverBinder) {
				
				fileObserverService = ((FileObserverBinder) service).getNotificationService();
				fileObserverService.registerNotificationListener(BoundServiceExampleFragment.this);
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.day5_fragment_bound_service, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		
		button = (Button) getView().findViewById(R.id.button_create);
		textView = (TextView) getView().findViewById(R.id.text_view);
		
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String name = String.valueOf(System.currentTimeMillis()) + ".txt";
				File file = new File(FileObserverService.OBSERVE_PATH, name);
				try {
					file.createNewFile();
				} catch (IOException e) {
					BoundServiceExampleFragment.this.notify(e.getMessage());
				}
			}
		});
	}
	
	@Override
	public void onStart() {
		
		super.onStart();
		getActivity().bindService(new Intent(getActivity(), FileObserverService.class), conn, Context.BIND_AUTO_CREATE);
	}
	
	@Override
	public void onStop() {
		
		super.onStop();
		getActivity().unbindService(conn);
	}

	@Override
	public void notify(final String message) {
		
		textView.post(new Runnable() {
			@Override
			public void run() {
				
				textView.append("\n" + message);
			}
		});
	}
}
