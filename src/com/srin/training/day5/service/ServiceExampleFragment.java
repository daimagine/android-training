package com.srin.training.day5.service;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.srin.training.R;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ServiceExampleFragment extends Fragment {
	
	private static final String TAG = "SRIN ServiceExampleFragment";
	private TextView mTextViewOutput;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
			mTextViewOutput.setText(sdf.format(intent.getSerializableExtra("date")));
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		showDebugLog("onCreateView");
		return inflater.inflate(R.layout.day5_fragment_service, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mTextViewOutput = (TextView) getView().findViewById(R.id.textview_output);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		showDebugLog("onStart");
		// first register the broadcast receiver
		getActivity().registerReceiver(receiver, new IntentFilter("DateTimeServiceAction"));
		// this will start the service
		getActivity().startService(new Intent(getActivity(), DateTimeService.class));
	}
	
	@Override
	public void onStop() {
		super.onStop();
		showDebugLog("onStop");
		// this will stop the service
		getActivity().stopService(new Intent(getActivity(), DateTimeService.class));
		// never forget to unregister the receiver
		getActivity().unregisterReceiver(receiver);
	}
	
	private void showDebugLog(String method) {
		showDebugLog(method, null);
	}
	
	private void showDebugLog(String method, String message) {
		Log.d(TAG, getClass().getName() + "." + method
				+ (message == null ? "" : " : " + message));
	}
	
}
