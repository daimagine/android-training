package com.srin.training.day5.service;

import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class DateTimeService extends Service {

	private boolean mRunning;
	private Thread mRunningThread;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		mRunning = true;
		mRunningThread = new Thread() {
			
			@Override
			public void run() {
				while(mRunning) {
					Intent intent = new Intent("DateTimeServiceAction");
					intent.putExtra("date", new Date());
					sendBroadcast(intent);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
						// stop running if thread is interrupted
						mRunning = false;
					}
				}
			}
			
		};
		mRunningThread.start();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mRunning = false;
		mRunningThread.interrupt();
	}

}
