package com.srin.training.day5.service;

import com.srin.training.R;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class BroadcastExampleFragment extends Fragment {

	private EditText mEditTextMessage;
	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Toast.makeText(context, intent.getStringExtra("message"),
					Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.day5_fragment_broadcast, container,
				false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mEditTextMessage = (EditText) getView().findViewById(R.id.edit_text_message);
		
		getView().findViewById(R.id.button_broadcast).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent("MessageBroadcast");
				intent.putExtra("message", mEditTextMessage.getText().toString());
				getActivity().sendBroadcast(intent);
			}
		});
	}
	
	@Override
	public void onStart() {
		super.onStart();
		getActivity().registerReceiver(receiver, new IntentFilter("MessageBroadcast"));
	}
	
	@Override
	public void onStop() {
		super.onStop();
		getActivity().unregisterReceiver(receiver);
	}
}
