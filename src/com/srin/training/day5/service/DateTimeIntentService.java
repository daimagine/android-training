package com.srin.training.day5.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import java.util.Date;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class DateTimeIntentService extends IntentService {

	private static final String TAG = "SRIN " + DateTimeIntentService.class.getSimpleName();
	public static final String DATE = "date_key";
	public static final String SERVICE_ACTION = "DateTimeIntentServiceAction";
	
	public DateTimeIntentService() {
		super("DateTimeIntentService");
		Log.d(TAG, "DateTimeIntentService init");
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand");
	    Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
	    return super.onStartCommand(intent,flags,startId);
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "onHandleIntent");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			stopSelf();
		}
		Log.d(TAG, "fetch time");
		RestAdapter adapter = new RestAdapter.Builder()
									.setServer(DateJsonTestService.SERVER)
									.build();
		DateJsonTestService jsonService = adapter.create(DateJsonTestService.class);
		DateResponse dateResponse = jsonService.getDate();
		
		Log.d(TAG, "date from server " + dateResponse.date);
		Log.d(TAG, "time from server " + dateResponse.milliseconds_since_epoch);
		
		Intent broadcastIntent = new Intent(SERVICE_ACTION);
		broadcastIntent.putExtra(DATE, new Date(dateResponse.milliseconds_since_epoch));
		sendBroadcast(broadcastIntent);
		Log.d(TAG, "send broadcast response. action " + broadcastIntent.getAction());
		
		Handler handler = new Handler(getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(DateTimeIntentService.this, "send broadcast response", Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	static interface DateJsonTestService {
		public static final String SERVER = "http://time.jsontest.com";
		public static final String DATE_FORMAT = "dd-MM-yyyy";
		@GET("/")
		DateResponse getDate();
	}
	
	static class DateResponse {
		String time;
		String date;
		long milliseconds_since_epoch;
	}

}
