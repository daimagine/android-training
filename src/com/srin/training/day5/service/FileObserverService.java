package com.srin.training.day5.service;

import java.util.HashSet;
import java.util.Set;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.FileObserver;
import android.os.IBinder;

public class FileObserverService extends Service {
	
	public static final String OBSERVE_PATH = "/sdcard/";
	
	public interface NotificationListener {
		
		void notify(String message);
	}
	
	public class FileObserverBinder extends Binder {
		
		public FileObserverService getNotificationService() {
			
			return FileObserverService.this;
		}
	}
	
	private FileObserverBinder binder;
	private Set<NotificationListener> listeners;
	private FileObserver observer;
	
	@Override
	public void onCreate() {
		
		binder = new FileObserverBinder();
		listeners = new HashSet<FileObserverService.NotificationListener>();
		
		observer = new FileObserver(OBSERVE_PATH) {
            @Override
            public void onEvent(int event, String file) {
            	
            	switch (event) {
				case FileObserver.CREATE:
	            	FileObserverService.this.notifyAll("file (" + file  + ") created.");
					break;
				case FileObserver.DELETE:
	            	FileObserverService.this.notifyAll("file (" + file  + ") deleted.");
					break;
				case FileObserver.MOVED_TO:
	            	FileObserverService.this.notifyAll("file (" + file  + ") added.");
					break;
				default:
					break;
				}
            }
        };
        observer.startWatching();
	}

	@Override
	public IBinder onBind(Intent intent) {
		
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {

		super.onDestroy();
		observer.stopWatching();
		observer = null;
	}
	
	public void registerNotificationListener(NotificationListener listener) {
		
		listeners.add(listener);
	}
	
	public void unregisterNotificationListener(NotificationListener listener) {
		
		listeners.remove(listener);
	}
	
	private void notifyAll(String message) {
		
		for (NotificationListener listener : listeners) {
			
			listener.notify(message);
		}
	}
}
