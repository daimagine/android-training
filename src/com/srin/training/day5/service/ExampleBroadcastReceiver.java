package com.srin.training.day5.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ExampleBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Toast.makeText(
				context,
				"ExampleBroadcastReceiver receive message: "
						+ intent.getStringExtra("message"), Toast.LENGTH_SHORT)
				.show();
	}

}
