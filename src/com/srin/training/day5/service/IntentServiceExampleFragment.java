package com.srin.training.day5.service;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import com.srin.training.R;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class IntentServiceExampleFragment extends Fragment {

	private static final String TAG = "SRIN " + IntentServiceExampleFragment.class.getSimpleName();
	private TextView mTextViewOutput;
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "onReceive broadcast action " + intent.getAction());
			Toast.makeText(getActivity(), "receive broadcast response", Toast.LENGTH_SHORT).show();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
			Date dateTime = (Date) intent.getSerializableExtra(DateTimeIntentService.DATE); 
			String dateStr = sdf.format(dateTime);
			Log.d(TAG, "date : " + dateStr);
			mTextViewOutput.setText(dateStr);
		}
	};
	
	public IntentServiceExampleFragment() {}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, "onActivityCreated");
		mTextViewOutput = (TextView) getView().findViewById(R.id.textview_output);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		return inflater.inflate(R.layout.day5_fragment_service, container, false);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart");
		Log.d(TAG, "register broadcast");
		getActivity().registerReceiver(mReceiver, 
				new IntentFilter(DateTimeIntentService.SERVICE_ACTION));
		Log.d(TAG, "start intent service");
		getActivity().startService(new Intent(getActivity(), DateTimeIntentService.class));
	}
	
	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
		Log.d(TAG, "stop service");
		getActivity().stopService(new Intent(getActivity(), DateTimeIntentService.class));
		Log.d(TAG, "unregister broadcast");
		getActivity().unregisterReceiver(mReceiver);
	}
}
