package com.srin.training.day9;

import java.util.Calendar;

import com.srin.training.MainActivity;
import com.srin.training.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.widget.ToggleButton;

public class NotificationAndAlarmExampleActivity extends Activity {
	
	// magic number, up 2 u
	private static final int NOTIFICATION_ID = 0x9987;
	private static final String TAG = NotificationAndAlarmExampleActivity.class.getSimpleName();
	
	public static final String ALARM_RECEIVE = "activity_alarm_receive";
	
	private ToggleButton mRepeatButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		
		setContentView(R.layout.day9_activity_notif_example);
		
		alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		
		findViewById(R.id.button_simple).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						showNotification("Open this application");
					}
				});
		findViewById(R.id.button_custom).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						showCustomNotification(R.layout.text_list_item, R.id.text_view, "Open this application [custom]");
					}
				});
		
		findViewById(R.id.button_alarm_once).setOnClickListener(
				new OnClickListener() {
			
					@Override
					public void onClick(View v) {
						setAlarm();
					}
				});
		
		mRepeatButton = (ToggleButton) findViewById(R.id.button_alarm_repeat);
		
		mRepeatButton.setOnClickListener(
				new OnClickListener() {
			
					@Override
					public void onClick(View v) {
						if (isRepeatingAlarmActive()) {
							mRepeatButton.setChecked(false);
							turnOffAlarm();
						} else {
							mRepeatButton.setChecked(true);
							setRepeatAlarm();
						}
					}
				});
		
		if (isRepeatingAlarmActive()) {
			Log.d(TAG, "notif alarm is active");
			mRepeatButton.setChecked(true);
		} else {
			mRepeatButton.setChecked(false);
		}
		
		if (getIntent().getBooleanExtra(ALARM_RECEIVE, false)) {
			Log.d(TAG, "call from notif alarm");
			Toast.makeText(this, "Call from notification alarm", Toast.LENGTH_LONG).show();
		}
	}

	private void showNotification(String message) {
		Notification.Builder builder = new Notification.Builder(
				getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(getString(R.string.app_name))
				.setContentText(message)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_ALL)
				.setTicker(message);
		Intent intent = new Intent(this, NotificationAndAlarmExampleActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}
	
	private void showCustomNotification(int layoutId, int viewId, String message) {
		RemoteViews views = new RemoteViews(getPackageName(), layoutId);
		views.setTextViewText(viewId, message);
		Notification.Builder builder = new Notification.Builder(
				getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
				.setAutoCancel(true)
				.setContent(views)
				.setDefaults(Notification.DEFAULT_ALL)
				.setTicker(message);
		Intent intent = new Intent(this, NotificationAndAlarmExampleActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	private AlarmManager alarmManager;
	private PendingIntent alarmIntent;
	
	private void setAlarm() {
		Log.d(TAG, "setAlarm");
		Intent intent = new Intent(AlarmReceiver.ACTION);
		alarmIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5 * 1000, alarmIntent);
	}
	
	private void setRepeatAlarm() {
		Log.d(TAG, "setRepeatAlarm");
		Intent intent = new Intent(AlarmReceiver.ACTION_REPEAT);
		alarmIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				SystemClock.elapsedRealtime() + 10 * 1000, 
				10 * 1000, alarmIntent);
	}
	
	private void turnOffAlarm() {
		if (alarmManager != null) {
			Intent intent = new Intent(AlarmReceiver.ACTION_REPEAT);
			alarmIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

			Log.d(TAG, "turnOffAlarm");
			alarmManager.cancel(alarmIntent);
			alarmIntent.cancel();
		}
	}
	
	private boolean isRepeatingAlarmActive() {
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(AlarmReceiver.ACTION_REPEAT), PendingIntent.FLAG_NO_CREATE);
		boolean active = (pendingIntent != null);
		Log.d(TAG, "isRepeatingAlarmActive " + active);
		return active;
	}
	
}
