package com.srin.training.day9;

import com.srin.training.MainActivity;
import com.srin.training.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	private static final String TAG = AlarmReceiver.class.getSimpleName();

	public static final String ACTION = AlarmReceiver.class.getName()
			+ ".ALARM_ACTION";
	public static final String ACTION_REPEAT = AlarmReceiver.class.getName()
			+ ".ALARM_ACTION_REPEAT";
	// magic number, up 2 u
	private static final int NOTIFICATION_ID = 0x9986;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive action " + intent.getAction());
		showNotification(context, intent.getAction());
	}

	private void showNotification(Context context, String action) {
		Log.d(TAG, "showNotification");
		Intent intent = new Intent(context,
				NotificationAndAlarmExampleActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(NotificationAndAlarmExampleActivity.ALARM_RECEIVE, true);

		Notification.Builder builder = new Notification.Builder(context)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(context.getString(R.string.app_name))
				.setContentText("Alarm has been triggered").setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_ALL).setTicker("Alarm triggered");
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

}
