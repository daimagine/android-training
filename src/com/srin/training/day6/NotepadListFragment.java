package com.srin.training.day6;

import android.app.Activity;
import android.app.Fragment;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.srin.training.R;
import com.srin.training.day6.database.NotepadContract.NotepadTable;
import com.srin.training.day6.provider.NotepadProvider;

public class NotepadListFragment extends Fragment {
	
	private OnListItemClickListener onListItemClickListener;
	
	private Activity activity;
	private ListView listView;
	private NotepadAdapter notepadAdapter;
	
	private Handler handler = new Handler();
	private ContentObserver observer = new ContentObserver(handler) {
		
		@Override
		public void onChange(boolean selfChange, Uri uri) {

			load();
		}
	};
	
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		
		this.activity = activity;
		
		if (activity instanceof OnListItemClickListener) {
			
			onListItemClickListener = (OnListItemClickListener) activity;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.day6_fragment_notepad_list, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		
		notepadAdapter = new NotepadAdapter(activity);
		
		listView = (ListView) getView().findViewById(R.id.list_view);
		listView.setAdapter(notepadAdapter);
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				onListItemClickListener.onItemClick(id);
			}
		});
	}
	
	@Override
	public void onResume() {

		super.onResume();
		
		activity.getContentResolver().registerContentObserver(NotepadProvider.CONTENT_URI, true, observer);
		
		load();
	}
	
	@Override
	public void onPause() {

		super.onPause();
		
		activity.getContentResolver().unregisterContentObserver(observer);
	}
	
	@Override
	public void onDetach() {

		super.onDetach();

		activity = null;
		onListItemClickListener = null;
	}
	
	private void load() {

		String URL = "content://com.srin.training.provider.Notepad/notepad";
		Uri uri = Uri.parse(URL);
		
		String[] projection = { NotepadTable._ID,
				NotepadTable.COLUMN_NAME_TITLE,
				NotepadTable.COLUMN_NAME_CONTENT 
				};
		
		Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
		
		notepadAdapter.swapCursor(cursor);
	}
	
	public interface OnListItemClickListener {
		
		void onItemClick(long id);
	}
}
