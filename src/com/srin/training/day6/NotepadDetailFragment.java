package com.srin.training.day6;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.srin.training.R;
import com.srin.training.day6.database.NotepadContract.NotepadTable;
import com.srin.training.day6.database.NotepadDataSource;
import com.srin.training.day6.database.entity.Notepad;

public class NotepadDetailFragment extends Fragment implements View.OnClickListener {

	public static final String SELECTED_NOTEPAD = "selected_notepad";
	
	private Activity activity;
	private NotepadDataSource notepadDataSource;

	private Button saveButton;
	private Button deleteButton;
	private EditText titleText;
	private EditText contentText;

	private long selectedNotepad;
	
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		
		this.activity = activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		notepadDataSource = new NotepadDataSource(activity);
		selectedNotepad = getArguments().getLong(SELECTED_NOTEPAD, 0L);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.day6_fragment_notepad_detail, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		saveButton = (Button) getView().findViewById(R.id.button_save);
		deleteButton = (Button) getView().findViewById(R.id.button_delete);
		titleText = (EditText) getView().findViewById(R.id.text_view_title);
		contentText = (EditText) getView().findViewById(R.id.text_view_content);
		
		saveButton.setOnClickListener(this);
		deleteButton.setOnClickListener(this);
		
		if (selectedNotepad > 0L) {

			Notepad notepad = notepadDataSource.getObject(selectedNotepad);
			titleText.setText(notepad.getTitle());
			contentText.setText(notepad.getContent());
			
			saveButton.setVisibility(View.GONE);
			
		} else {
			
			deleteButton.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onDetach() {

		super.onDetach();
		
		activity = null;
	}

	@Override
	public void onClick(View v) {
		
		if (activity.getCurrentFocus() != null) {
			
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		}
		
		switch (v.getId()) {
		case R.id.button_save:
			onSave();
			break;
		case R.id.button_delete:
			onDelete();
			break;
		default:
			break;
		}
	}
	
	private void onSave() {
		
		ContentValues values = new ContentValues();
		values.put(NotepadTable.COLUMN_NAME_TITLE, titleText.getText().toString());
		values.put(NotepadTable.COLUMN_NAME_CONTENT, contentText.getText().toString());
		
		String URL = "content://com.srin.training.provider.Notepad/notepad";
		Uri uri = Uri.parse(URL);
		
		activity.getContentResolver().insert(uri, values);
	}
	
	private void onDelete() {
		
		if (selectedNotepad > 0L) {

			String URL = "content://com.srin.training.provider.Notepad/notepad/" + selectedNotepad;
			Uri uri = Uri.parse(URL);
			
			activity.getContentResolver().delete(uri, null, null);
		}
	}
}
