package com.srin.training.day6;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.srin.training.R;
import com.srin.training.day6.provider.NotepadProvider;

public class NotepadActivity extends Activity implements NotepadListFragment.OnListItemClickListener {
	
	private long selectedNotepad;
	
	private Handler handler = new Handler();
	
	private ContentObserver observer = new ContentObserver(handler) {
		
		@Override
		public void onChange(boolean selfChange, Uri uri) {

			selectedNotepad = 0L;

			if (findViewById(R.id.detail_layout) != null) {

				openDetail();
				
			} else {
				
				openList();
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null) {
			
			if (savedInstanceState.containsKey(NotepadDetailFragment.SELECTED_NOTEPAD)) {
				
				selectedNotepad = savedInstanceState.getLong(NotepadDetailFragment.SELECTED_NOTEPAD);
			}
		}
		
		init();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
		
		init();
		invalidateOptionsMenu();
	}
	
	@Override
	protected void onResume() {

		super.onResume();
		
		getContentResolver().registerContentObserver(NotepadProvider.CONTENT_URI, true, observer);
	}
	
	@Override
	protected void onPause() {

		super.onPause();
		
		getContentResolver().unregisterContentObserver(observer);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		
		outState.putLong(NotepadDetailFragment.SELECTED_NOTEPAD, selectedNotepad);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (findViewById(R.id.detail_layout) == null) {

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.day6_menu_notepad, menu);
		}
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == R.id.action_create) {
			
			selectedNotepad = 0L;
			openDetail();
			
			return true;
		}
		
		return false;
	}
	
	@Override
	public void onItemClick(long id) {
		
		selectedNotepad = id;
		openDetail();
	}
	
	private void init() {
		
		setContentView(R.layout.day6_activity_notepad);
		
		if (findViewById(R.id.detail_layout) != null) {
			
			openList();
			openDetail();
			
		} else if (selectedNotepad > 0L) {
			
			openDetail();
			
		} else {
			
			openList();
		}
	}
	
	private void openList() {
		
		Bundle bundle = getIntent().getExtras();
		
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		NotepadListFragment listFragment = new NotepadListFragment();
		listFragment.setArguments(bundle);
		
		FragmentManager manager = getFragmentManager();
		
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.replace(R.id.master_layout, listFragment).commit();
	}
	
	private void openDetail() {
			
		Bundle bundle = getIntent().getExtras();
		
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putLong(NotepadDetailFragment.SELECTED_NOTEPAD, selectedNotepad);
		
		NotepadDetailFragment detailFragment = new NotepadDetailFragment();
		detailFragment.setArguments(bundle);
		
		FragmentManager manager = getFragmentManager();
		
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		
		if (findViewById(R.id.detail_layout) != null) {
			
			transaction.replace(R.id.detail_layout, detailFragment);
			
		} else {
			
			transaction.replace(R.id.master_layout, detailFragment);
		}
		
		transaction.commit();
	}
}
