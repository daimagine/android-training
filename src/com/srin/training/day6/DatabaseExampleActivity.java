package com.srin.training.day6;

import java.util.HashMap;

import com.srin.training.R;
import com.srin.training.day4.MasterMenuFragment;
import com.srin.training.day5.service.BoundServiceExampleFragment;
import com.srin.training.day5.service.BroadcastExampleFragment;
import com.srin.training.day5.service.IntentServiceExampleFragment;
import com.srin.training.day5.service.ServiceExampleFragment;
import com.srin.training.day6.database.SQLiteDatabaseExampleFragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

public class DatabaseExampleActivity extends Activity implements
		MasterMenuFragment.TopicSelectedListener {

	private static final String TAG = "SRIN DatabaseExampleActivity";
	private static final String KEY_LAST_TOPIC = "key_last_topic";

	private int mLastTopic;
	private boolean mDualPane;

	private static final int MASTER_MENU_FRAGMENT = 0;
	private static final int MENU_DATABASE = 1;

	@SuppressLint("UseSparseArrays")
	private static final HashMap<Integer, String> MENUS = new HashMap<Integer, String>();

	static {
		MENUS.put(MENU_DATABASE, "SQLite Database");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showDebugLog("onCreate");
		setContentView(R.layout.activity_master_detail);
		mDualPane = findViewById(R.id.detail_layout) != null;
		if (savedInstanceState != null) {
			mLastTopic = savedInstanceState.getInt(KEY_LAST_TOPIC,
					MASTER_MENU_FRAGMENT);

			showDebugLog("onCreate", "Last Topic: " + mLastTopic);
			if (mLastTopic != MASTER_MENU_FRAGMENT && mDualPane) {
				onTopicSelected(MASTER_MENU_FRAGMENT);
			}
		} else {
			mLastTopic = MASTER_MENU_FRAGMENT;
		}
		onTopicSelected(mLastTopic);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_LAST_TOPIC, mLastTopic);
	}

	@Override
	public void onTopicSelected(int topic) {
		showDebugLog("onTopicSelected", "topic: " + topic);
		int layoutId = mDualPane ? R.id.detail_layout : R.id.master_layout;
		Fragment fragment = null;
		switch (topic) {
		case MASTER_MENU_FRAGMENT:
			layoutId = R.id.master_layout;
			fragment = findFragment(MasterMenuFragment.class, layoutId);
			// this will prevent same fragment being recreated
			if (fragment == null) {
				fragment = new MasterMenuFragment();
				Bundle bundle = new Bundle();
				bundle.putSerializable(MasterMenuFragment.MENUS_KEY, MENUS);
				fragment.setArguments(bundle);
			}
			break;

		case MENU_DATABASE:
			fragment = findFragment(SQLiteDatabaseExampleFragment.class, layoutId);
			if (fragment == null) {
				fragment = new SQLiteDatabaseExampleFragment();
			}
			break;
		}

		if (fragment == null) {
			throw new RuntimeException("Invalid Topic Selected");
		}

		// this will prevent the manager to replace same fragment object
		if (fragment != getFragmentManager().findFragmentById(layoutId)) {
			Fragment prevRightFragment = getFragmentManager().findFragmentById(
					R.id.detail_layout);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			// this to ensure right fragment to be removed when not in dual pane
			// to ensure method in fragment life cycle not called twice
			if (!mDualPane && prevRightFragment != null) {
				ft.remove(prevRightFragment);
			}
			ft.replace(layoutId, fragment, fragment.getClass().getName())
					.commit();
		}

		if (topic != MASTER_MENU_FRAGMENT) {
			mLastTopic = topic;
		}
	}

	private Fragment findFragment(Class<? extends Fragment> fragmentClass,
			int id) {
		Fragment fragment = getFragmentManager().findFragmentByTag(
				fragmentClass.getName());
		if (fragment != null && fragment.getId() == id) {
			return fragment;
		}
		return null;
	}

	private void showDebugLog(String method) {
		showDebugLog(method, null);
	}

	private void showDebugLog(String method, String message) {
		Log.d(TAG, getClass().getName() + "." + method
				+ (message == null ? "" : " : " + message));
	}

}
