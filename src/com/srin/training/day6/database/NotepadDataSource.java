package com.srin.training.day6.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.srin.training.day6.database.NotepadContract.NotepadTable;
import com.srin.training.day6.database.entity.Notepad;

public class NotepadDataSource {

	private SQLiteDatabase db;
	
	public NotepadDataSource(Context context) {
		
		this(new NotepadDbHelper(context));
	}
	
	public NotepadDataSource(NotepadDbHelper notepadDbHelper) {

		db = notepadDbHelper.getWritableDatabase();
	}
	
	public Cursor getAllInCursor() {
		
		String[] projection = { NotepadTable._ID,
				NotepadTable.COLUMN_NAME_TITLE,
				NotepadTable.COLUMN_NAME_CONTENT };
		
		return db.query(NotepadTable.TABLE_NAME, projection, null, null, null, null, null);
	}
	
	public List<Notepad> getAllInList() {
		
		List<Notepad> notepads = new ArrayList<Notepad>();
		
		Cursor cursor = getAllInCursor();
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			
			Notepad notepad = new Notepad();
			notepad.setId(cursor.getLong(cursor.getColumnIndex(NotepadTable._ID)));
			notepad.setTitle(cursor.getString(cursor.getColumnIndex(NotepadTable.COLUMN_NAME_TITLE)));
			notepad.setContent(cursor.getString(cursor.getColumnIndex(NotepadTable.COLUMN_NAME_CONTENT)));
			
			notepads.add(notepad);
		}
		
		cursor.close();
		
		return notepads;
	}
	
	public Cursor getCursor(long id) {
		
		String[] projection = {
				NotepadTable._ID,
				NotepadTable.COLUMN_NAME_TITLE,
				NotepadTable.COLUMN_NAME_CONTENT
		    };
		
		String selection = NotepadTable._ID + " = ?";
		
		String[] selectionArgs = {
				String.valueOf(id)
		    };
		
		return db.query(NotepadTable.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
	}
	
	public Notepad getObject(long id) {
		
		Cursor cursor = getCursor(id);
		
		if (cursor.getCount() > 0) {
			
			cursor.moveToFirst();
			
			Notepad notepad = new Notepad();
			notepad.setId(cursor.getLong(cursor.getColumnIndex(NotepadTable._ID)));
			notepad.setTitle(cursor.getString(cursor.getColumnIndex(NotepadTable.COLUMN_NAME_TITLE)));
			notepad.setContent(cursor.getString(cursor.getColumnIndex(NotepadTable.COLUMN_NAME_CONTENT)));
			
			return notepad;
		}
		
		cursor.close();
		
		return null;
	}
	
	public void create(Notepad notepad) {
		
		ContentValues values = new ContentValues();
		values.put(NotepadTable.COLUMN_NAME_TITLE, notepad.getTitle());
		values.put(NotepadTable.COLUMN_NAME_CONTENT, notepad.getContent());
		
		create(values);
	}
	
	public void create(ContentValues values) {
		
		db.insert(NotepadTable.TABLE_NAME, null, values);
	}
	
	public void delete(long id) {
		
		String selection = NotepadTable._ID + " = ?";
		String[] selectionArgs = { String.valueOf(id) };

		db.delete(NotepadTable.TABLE_NAME, selection, selectionArgs);
	}
}
