package com.srin.training.day6.database;

public class DataModel {
	
	private long mId;
	private String mName;
	private String mDescription;
	
	public DataModel() {
		this(0, null, null);
	}
	
	public DataModel(int id, String name, String description) {
		mId = id;
		mName = name;
		mDescription = description;
	}
	
	public long getId() {
		return mId;
	}
	
	void setId(long id) {
		mId = id;
	}
	
	public void setName(String name) {
		mName = name;
	}
	
	public String getName() {
		return mName;
	}
	
	public void setDescription(String description) {
		mDescription = description;
	}
	
	public String getDescription() {
		return mDescription;
	}

}
