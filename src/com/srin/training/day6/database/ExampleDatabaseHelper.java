package com.srin.training.day6.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ExampleDatabaseHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "ExampleDB";
	private static final String TABLE_1_NAME = "Table1";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_DESCRIPION = "description";

	public ExampleDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_1_NAME + " (" + COLUMN_NAME
				+ " TEXT," + COLUMN_DESCRIPION + " TEXT )");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// do nothing on first version
	}

	public Cursor getAllData() {
		SQLiteDatabase db = getReadableDatabase();
		return db.query(TABLE_1_NAME, new String[] { "rowid _id", COLUMN_NAME,
				COLUMN_DESCRIPION }, null, null, null, null, null);
	}

	public void addData(DataModel data) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COLUMN_NAME, data.getName());
		contentValues.put(COLUMN_DESCRIPION, data.getDescription());
		long id = db.insertOrThrow(TABLE_1_NAME, null, contentValues);
		data.setId(id);
	}

	public boolean removeData(long id) {
		SQLiteDatabase db = getWritableDatabase();
		return db.delete(TABLE_1_NAME, "rowid=?",
				new String[] { String.valueOf(id) }) > 0;
	}
}
