package com.srin.training.day6.database;

import com.srin.training.R;
import com.srin.training.day3.arrayadapter.Data;
import com.srin.training.day3.arrayadapter.DataAdapter.ViewHolder;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class DatabaseCursorAdapter extends CursorAdapter {

	public DatabaseCursorAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();
		holder.nameText.setText(cursor.getString(cursor.getColumnIndex("name")));
		holder.descText.setText(cursor.getString(cursor.getColumnIndex("description")));
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View convertView = inflater.inflate(R.layout.custom_list_item, parent, false);
		ViewHolder holder = new ViewHolder();
		holder.nameText = (TextView) convertView.findViewById(R.id.name);
		holder.descText = (TextView) convertView.findViewById(R.id.description);
		convertView.setTag(holder);
		return convertView;
	}
	
	public static class ViewHolder {
		TextView nameText;
		TextView descText;
	}
}
