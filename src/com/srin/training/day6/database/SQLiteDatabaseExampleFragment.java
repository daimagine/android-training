package com.srin.training.day6.database;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;

import com.srin.training.R;
import com.srin.training.day3.InputActivity;

public class SQLiteDatabaseExampleFragment extends Fragment {

	private static final String TAG = "SRIN SQLiteDatabaseExampleFragment";
	private static final int INPUT_REQUEST_CODE = 0;
	private ListView mListView;
	private CursorAdapter mAdapter;
	private ExampleDatabaseHelper mDatabaseHelper;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		showDebugLog("onCreateView");
		return inflater.inflate(R.layout.contact_list_with_add_layout, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		showDebugLog("onActivityCreated");
		mDatabaseHelper = new ExampleDatabaseHelper(getActivity());
		mListView = (ListView) getView().findViewById(R.id.list_view);
		reloadList();
		getView().findViewById(R.id.button_add).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(), InputActivity.class), INPUT_REQUEST_CODE);
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case INPUT_REQUEST_CODE:
			if(resultCode == Activity.RESULT_OK) {
				new AsyncTask<Void, Void, Cursor>() {

					private ProgressDialog mProgressDialog;
					
					protected void onPreExecute() {
						mProgressDialog = ProgressDialog.show(getActivity(), null, "Loading");
					}
					
					@Override
					protected Cursor doInBackground(Void... params) {
						DataModel dataModel = new DataModel();
						dataModel.setName(data.getStringExtra("name"));
						dataModel.setDescription(data.getStringExtra("description"));
						mDatabaseHelper.addData(dataModel);
						return mDatabaseHelper.getAllData();
					}
					
					protected void onPostExecute(Cursor result) {
						mAdapter.changeCursor(result);
						mProgressDialog.dismiss();
					}
					
				}.execute();
			}
			break;
		}
	}
	
	@Override
	public void onStart() {
		super.onStart();
		showDebugLog("onStart");
	}
	
	@Override
	public void onStop() {
		super.onStop();
		showDebugLog("onStop");
	}
	
	private void reloadList() {
		new AsyncTask<Void, Void, Cursor>() {

			private ProgressDialog mProgressDialog;
			
			protected void onPreExecute() {
				mProgressDialog = ProgressDialog.show(getActivity(), null, "Loading");
			}
			
			@Override
			protected Cursor doInBackground(Void... params) {
				return mDatabaseHelper.getAllData();
			}
			
			protected void onPostExecute(Cursor result) {
				if(mAdapter == null) {
					mAdapter = new DatabaseCursorAdapter(getActivity(), result, false);
				}
				mListView.setAdapter(mAdapter);
				mProgressDialog.dismiss();
			}
			
		}.execute();
	}
	
	private void showDebugLog(String method) {
		showDebugLog(method, null);
	}
	
	private void showDebugLog(String method, String message) {
		Log.d(TAG, getClass().getName() + "." + method
				+ (message == null ? "" : " : " + message));
	}
	
}
