package com.srin.training.day6.database;

import android.provider.BaseColumns;

public final class NotepadContract {

	private NotepadContract() { }
	
	public static abstract class NotepadTable implements BaseColumns {
		
		public static final String TABLE_NAME = "notepad";
		public static final String COLUMN_NAME_TITLE = "title";
		public static final String COLUMN_NAME_CONTENT = "content";
	}
}
