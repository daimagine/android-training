package com.srin.training.day6.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.srin.training.day6.database.NotepadContract.NotepadTable;
import com.srin.training.day6.database.NotepadDbHelper;

public class NotepadProvider extends ContentProvider {
	
	private static final String AUTHORITY = "com.srin.training.provider.Notepad";
	private static final String URL = "content://" + AUTHORITY + "/notepad";
	
	public static final Uri CONTENT_URI = Uri.parse(URL);
	
	static final int NOTEPAD_LIST = 1;
	static final int NOTEPAD = 2;

	static final UriMatcher URI_MATCHER;
	
	static {

		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URI_MATCHER.addURI(AUTHORITY, "notepad", NOTEPAD_LIST);
		URI_MATCHER.addURI(AUTHORITY, "notepad/#", NOTEPAD);
	}

	private SQLiteDatabase db;
	
	@Override
	public boolean onCreate() {
		
		NotepadDbHelper notepadDbHelper = new NotepadDbHelper(getContext());
		db = notepadDbHelper.getWritableDatabase();
		
		return true;
	}

	@Override
	public String getType(Uri uri) {

		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

		Cursor cursor = null;
		
		switch (URI_MATCHER.match(uri)) {
			case NOTEPAD_LIST:
				cursor = db.query(NotepadTable.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
				break;
		}

		return cursor;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		
		long id = 0;
		
		switch (URI_MATCHER.match(uri)) {
			case NOTEPAD_LIST:
				id = db.insert(NotepadTable.TABLE_NAME, null, values);
				break;
		}
		
		Uri result = CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
		
		if (id > 0) {
			
			getContext().getContentResolver().notifyChange(CONTENT_URI, null);
		}

		return result;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

		return 0;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {

		int result = 0;
		
		switch (URI_MATCHER.match(uri)) {
			case NOTEPAD:
				
				String where = NotepadTable._ID + " = ?";
				String[] args = { uri.getLastPathSegment() };
				result = db.delete(NotepadTable.TABLE_NAME, where, args);
				break;
		}
		
		if (result > 0) {
			
			getContext().getContentResolver().notifyChange(CONTENT_URI, null);
		}
		
		return result;
	}
}
