package com.srin.training.day6;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.srin.training.R;
import com.srin.training.day6.database.NotepadContract.NotepadTable;

public class NotepadAdapter extends CursorAdapter {

	public NotepadAdapter(Context context) {
		super(context, null, true);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {

		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.text_list_item, parent, false);
		
		ViewHolder holder = new ViewHolder();
		holder.textView = (TextView) view.findViewById(R.id.text_view);
		view.setTag(holder);
		
		return view;
	}
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		
		ViewHolder holder = (ViewHolder) view.getTag();
		
		if (cursor != null) {
			
			holder.textView.setText(cursor.getString(cursor.getColumnIndex(NotepadTable.COLUMN_NAME_TITLE)));
		}
	}

	static class ViewHolder {
		
		TextView textView;
	}
}
