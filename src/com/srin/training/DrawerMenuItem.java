package com.srin.training;

import java.util.List;

import android.app.Activity;

public interface DrawerMenuItem {

	MenuItem getGroupItem();
	
	List<MenuItem> getChildItem();
	
	class MenuItem {
		
		private String name;
		private Class<? extends Activity> activity;
		
		public MenuItem(String name, Class<? extends Activity> activity) {
			this.name = name;
			this.activity = activity;
		}

		public String getName() {
			return name;
		}
		
		public Class<? extends Activity> getActivity() {
			return activity;
		}
	}
}
