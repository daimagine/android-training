package com.srin.training.day7;

import com.srin.training.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class StylesThemesExampleActivity extends Activity {

	private static final String TAG = "SRIN DatabaseExampleActivity";
	private Button mButton;
	private int mButtonWidth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showDebugLog("onCreate");
		setContentView(R.layout.day7_activity_styles_themes_example);

		mButton = (Button) findViewById(R.id.button1);
		
		mButtonWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
		final LinearLayout.LayoutParams fillWidth = new LinearLayout.LayoutParams(
			      LinearLayout.LayoutParams.MATCH_PARENT, 
			      LinearLayout.LayoutParams.WRAP_CONTENT);
		final LinearLayout.LayoutParams wrapWidth = new LinearLayout.LayoutParams(
			      LinearLayout.LayoutParams.WRAP_CONTENT, 
			      LinearLayout.LayoutParams.WRAP_CONTENT);
		
		mButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDebugLog("button width : " + mButtonWidth);
				if (mButtonWidth == ViewGroup.LayoutParams.MATCH_PARENT) {
					mButtonWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
					mButton.setLayoutParams(wrapWidth);
				} else {
					mButtonWidth = ViewGroup.LayoutParams.MATCH_PARENT;
					mButton.setLayoutParams(fillWidth);
				}
			}
		});
	}

	private void showDebugLog(String method) {
		showDebugLog(method, null);
	}

	private void showDebugLog(String method, String message) {
		Log.d(TAG, getClass().getSimpleName() + "." + method
				+ (message == null ? "" : " : " + message));
	}
}
