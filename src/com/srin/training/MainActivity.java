package com.srin.training;

import java.util.ArrayList;
import java.util.List;

import com.srin.training.day2.MasterDetailActivity;
import com.srin.training.day3.ListViewActivity;
import com.srin.training.day3.ExpandableListActivity;
import com.srin.training.day4.DialogExampleActivity;
import com.srin.training.day4.FourthMasterActivity;
import com.srin.training.day4.SharedPreferencesActivity;
import com.srin.training.day5.ServiceExampleActivity;
import com.srin.training.day6.NotepadActivity;
import com.srin.training.day6.DatabaseExampleActivity;
import com.srin.training.day7.StylesThemesExampleActivity;
import com.srin.training.day8.SettingsActivity;
import com.srin.training.day8.actionbar.ActionBarExampleActivity;
import com.srin.training.day9.NotificationAndAlarmExampleActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

public class MainActivity extends Activity {

	private ExpandableListView expandableListView;
	private DrawerMenuItemAdapter drawerMenuItemAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		expandableListView = (ExpandableListView) findViewById(R.id.left_drawer);

		final List<DrawerMenuItem> drawerMenuItems = new ArrayList<DrawerMenuItem>();
		drawerMenuItems.add(dayOneMenuItem);
		drawerMenuItems.add(dayTwoMenuItem);
		drawerMenuItems.add(dayThreeMenuItem);
		drawerMenuItems.add(dayFourMenuItem);
		drawerMenuItems.add(dayFiveMenuItem);
		drawerMenuItems.add(daySixMenuItem);
		drawerMenuItems.add(daySevenMenuItem);
		drawerMenuItems.add(dayEightMenuItem);
		drawerMenuItems.add(dayNineMenuItem);

		drawerMenuItemAdapter = new DrawerMenuItemAdapter(this, drawerMenuItems);
		expandableListView.setAdapter(drawerMenuItemAdapter);
		expandableListView
				.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

					@Override
					public boolean onChildClick(ExpandableListView parent,
							View v, int groupPosition, int childPosition,
							long id) {

						Class<?> activity = drawerMenuItems.get(groupPosition)
								.getChildItem().get(childPosition)
								.getActivity();

						Log.d("TRAINING",
								"MainActivity.onChildClick.activity : "
										+ activity);

						if (activity != null) {

							Intent intent = new Intent(MainActivity.this,
									activity);
							startActivity(intent);

							return true;
						}

						return false;
					}
				});
	}

	DrawerMenuItem dayOneMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 1", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Activity 1", null));
			items.add(new MenuItem("Activity 2", null));

			return items;
		}
	};

	DrawerMenuItem dayTwoMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 2", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Master Detail", MasterDetailActivity.class));

			return items;
		}
	};

	DrawerMenuItem dayThreeMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 3", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("List View", ListViewActivity.class));
			items.add(new MenuItem("Expandable List View",
					ExpandableListActivity.class));

			return items;
		}
	};

	DrawerMenuItem dayFourMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 4", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("AsyncTask", FourthMasterActivity.class));
			items.add(new MenuItem("Dialogs", DialogExampleActivity.class));
			items.add(new MenuItem("Shared Preferences",
					SharedPreferencesActivity.class));

			return items;
		}
	};

	DrawerMenuItem dayFiveMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 5", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Service and BroadcastReceiver",
					ServiceExampleActivity.class));
			return items;
		}
	};

	DrawerMenuItem daySixMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 6", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Notepad", NotepadActivity.class));
			items.add(new MenuItem("Database and Content Provider",
					DatabaseExampleActivity.class));
			return items;
		}
	};

	DrawerMenuItem daySevenMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 7", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Styles and Themes",
					StylesThemesExampleActivity.class));
			return items;
		}
	};

	DrawerMenuItem dayEightMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 8", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("ActionBar and Menu",
					ActionBarExampleActivity.class));
			items.add(new MenuItem(
					"ActionBar and Menu (AppCompat)",
					com.srin.training.day8.actionbarwithappcompat.ActionBarExampleActivity.class));
			items.add(new MenuItem("Settings", SettingsActivity.class));
			return items;
		}
	};

	DrawerMenuItem dayNineMenuItem = new DrawerMenuItem() {

		@Override
		public MenuItem getGroupItem() {
			return new MenuItem("Day 9", null);
		}

		@Override
		public List<MenuItem> getChildItem() {

			List<MenuItem> items = new ArrayList<MenuItem>();
			items.add(new MenuItem("Notification and Alarm",
					NotificationAndAlarmExampleActivity.class));
			return items;
		}
	};

}
