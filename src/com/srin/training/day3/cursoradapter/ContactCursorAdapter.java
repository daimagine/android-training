package com.srin.training.day3.cursoradapter;

import com.srin.training.R;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class ContactCursorAdapter extends CursorAdapter {
	
	private final Context mContext;

	public ContactCursorAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		this.mContext = context;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View view = inflater.inflate(R.layout.contact_list_item, parent,
				false);

		ViewHolder holder = new ViewHolder();
		holder.name = (TextView) view.findViewById(R.id.name);

		view.setTag(holder);
		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();
		if(cursor != null) {
			android.util.Log.e("SRIN", "cursor count : " + cursor.getCount());
			int nameIdx = cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME);
			android.util.Log.e("SRIN", "name : " + cursor.getString(nameIdx));
			String name = cursor.isNull(nameIdx) ? null : cursor.getString(nameIdx);
			holder.name.setText(name);
		}
	}

	static class ViewHolder {
		TextView name;
	}

}
