package com.srin.training.day3.cursoradapter;

import com.srin.training.R;
import com.srin.training.day3.ListViewActivity;
import com.srin.training.day3.MasterFragment.OnListItemClickListener;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ContactsFragment extends Fragment implements LoaderCallbacks<Cursor> {

	//adapter n list view
	private ListView mListView;
	private CursorAdapter mCursorAdapter;
	private boolean mLoadingCursor = true;
	
	//loader ID
	public static final int LOADER_ID_CONTACTS = 1;
	public static final String CRITERIA_NAME = "contact_criteria_name";
	public static final String FORCE_PROGRESSBAR = "contact_force_progressbar";
	
	private static final String TAG = ContactsFragment.class.getSimpleName();
	private ProgressDialog mProgressDialog;
	
	public ContactsFragment() {}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getLoaderManager().initLoader(LOADER_ID_CONTACTS, null, this);
		return inflater.inflate(R.layout.contact_list_layout, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, "onActivityCreated");
		Log.d(TAG, "Activity " + getActivity());
		Log.d(TAG, "Fragment " + this);
		mListView = (ListView) getView().findViewById(R.id.list_view);
		mCursorAdapter = new ContactCursorAdapter(getActivity(), null, false);

		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		Log.d(TAG, "mProgressDialog " + mProgressDialog);
		
		mListView.setAdapter(mCursorAdapter);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle arguments) {
		if(id == LOADER_ID_CONTACTS) {
			Log.d(TAG, "onCreateLoader");
			String selection = null;
			String[] selectionArgs = null;
			if (arguments != null) {
				if (arguments.containsKey(CRITERIA_NAME)
						&& arguments.getSerializable(CRITERIA_NAME) != null) {

					selection = ContactsContract.Contacts.DISPLAY_NAME
							+ " like ? ";
					selectionArgs = new String[1];
					selectionArgs[0] = "%" + arguments.getString(CRITERIA_NAME)
							+ "%";

					Log.d(TAG, "selection : " + selection);
					Log.d(TAG, "selection args : " + selectionArgs[0]);

					mLoadingCursor = false;
				}
				
				if (arguments.containsKey(FORCE_PROGRESSBAR)) {
					mLoadingCursor = arguments.getBoolean(FORCE_PROGRESSBAR, false);
				}
			}
			
			Log.d(TAG, "mLoadingCursor " + mLoadingCursor);
			if (mLoadingCursor && mProgressDialog != null) {
				Log.d(TAG, "mProgressDialog " + mProgressDialog);
				mProgressDialog.show();
			}
			
			String[] projection = {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
			
			return new CursorLoader(getActivity(), ContactsContract.Contacts.CONTENT_URI, 
					projection, selection, selectionArgs, null);
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		Log.d(TAG, "onLoadFinished");
		Log.d(TAG, "mProgressDialog " + mProgressDialog);
		
		if(cursor != null && cursor.getCount() > 0) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			mCursorAdapter.swapCursor(cursor);
			
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
		}
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		Log.d(TAG, "onLoaderReset");
		mCursorAdapter.swapCursor(null);

		Log.d(TAG, "mProgressDialog " + mProgressDialog);
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		Log.d(TAG, "onDetach");
		
		Log.d(TAG, "mProgressDialog " + mProgressDialog);
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}
}
