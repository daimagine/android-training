package com.srin.training.day3;

import android.app.Activity;
import android.os.Bundle;

import com.srin.training.R;
import com.srin.training.day3.MasterFragment.OnListItemClickListener;
import com.srin.training.day3.arrayadapter.ArrayAdapterWithCustomLayoutFragment;
import com.srin.training.day3.arrayadapter.ArrayAdapterWithSimpleLayoutFragment;
import com.srin.training.day3.cursoradapter.ContactsFragment;

public class ListViewActivity extends Activity implements
		OnListItemClickListener {

	private static final String KEY_LAST_FRAGMENT = "key_last_fragment";

	private static final int MASTER_FRAGMENT = 0;
	private static final int ARRAY_ADAPTER_WITH_SIMPLE_LIST_FRAGMENT = 1;
	private static final int ARRAY_ADAPTER_WITH_CUSTOM_LIST_FRAGMENT = 2;
	private static final int CURSOR_FRAGMENT = 3;

	private int mLastFragment;
	private boolean mInLandscapeMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_master_detail);
		mInLandscapeMode = findViewById(R.id.detail_layout) != null;
		if (savedInstanceState != null) {
			mLastFragment = savedInstanceState.getInt(KEY_LAST_FRAGMENT,
					MASTER_FRAGMENT);
			if (mLastFragment != MASTER_FRAGMENT && mInLandscapeMode) {
				showFragment(MASTER_FRAGMENT);
			}
		} else {
			mLastFragment = MASTER_FRAGMENT;
		}
		showFragment(mLastFragment);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_LAST_FRAGMENT, mLastFragment);
	}

	private void showFragment(int whichFragment) {
		int layoutId = mInLandscapeMode ? R.id.detail_layout
				: R.id.master_layout;
		switch (whichFragment) {
		case MASTER_FRAGMENT: {
			MasterFragment fragment = new MasterFragment();
			getFragmentManager().beginTransaction()
					.replace(R.id.master_layout, fragment).commit();
		}
			break;
		case ARRAY_ADAPTER_WITH_SIMPLE_LIST_FRAGMENT: {
			ArrayAdapterWithSimpleLayoutFragment fragment = new ArrayAdapterWithSimpleLayoutFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		case ARRAY_ADAPTER_WITH_CUSTOM_LIST_FRAGMENT: {
			ArrayAdapterWithCustomLayoutFragment fragment = new ArrayAdapterWithCustomLayoutFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		case CURSOR_FRAGMENT: {
			ContactsFragment fragment = new ContactsFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		}
		if (whichFragment != MASTER_FRAGMENT) {
			mLastFragment = whichFragment;
		}
	}

	@Override
	public void onListItemClick(String menu) {
		if (menu.equals("Cursor Adapter")) {
			showFragment(CURSOR_FRAGMENT);
		} else if (menu.equals("Array Adapter With Simple Layout")) {
			showFragment(ARRAY_ADAPTER_WITH_SIMPLE_LIST_FRAGMENT);
		} else if (menu.equals("Array Adapter With Custom Layout")) {
			showFragment(ARRAY_ADAPTER_WITH_CUSTOM_LIST_FRAGMENT);
		}
	}

}
