package com.srin.training.day3;

import com.srin.training.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InputActivity extends Activity {

	private EditText mNameEdit;
	private EditText mDescEdit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.day3_activity_input);
		mNameEdit = (EditText) findViewById(R.id.name);
		mDescEdit = (EditText) findViewById(R.id.description);
		findViewById(R.id.button_save).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent data = new Intent();
				data.putExtra("name", mNameEdit.getText().toString());
				data.putExtra("description", mDescEdit.getText().toString());
				setResult(RESULT_OK, data);
				finish();
			}
		});
	}
	
}
