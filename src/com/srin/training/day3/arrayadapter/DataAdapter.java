package com.srin.training.day3.arrayadapter;

import com.srin.training.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DataAdapter extends ArrayAdapter<Data> {

	public DataAdapter(Context context) {
		// create an array adapter with layout custom_list_item
		super(context, R.layout.custom_list_item);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.custom_list_item, parent, false);
			holder = new ViewHolder();
			holder.nameText = (TextView) convertView.findViewById(R.id.name);
			holder.descText = (TextView) convertView.findViewById(R.id.description);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Data data = getItem(position);
		holder.nameText.setText(data.getName());
		holder.descText.setText(data.getDescription());
		return convertView;
	}
	
	public static class ViewHolder {
		TextView nameText;
		TextView descText;
	}

}
