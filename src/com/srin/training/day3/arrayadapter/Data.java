package com.srin.training.day3.arrayadapter;

public class Data {
	
	private String mName;
	private String mDescription;
	
	public Data(String name, String description) {
		mName = name;
		mDescription = description;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getDescription() {
		return mDescription;
	}
	
}
