package com.srin.training.day3.arrayadapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.srin.training.R;
import com.srin.training.day3.InputActivity;

public class ArrayAdapterWithCustomLayoutFragment extends Fragment {
	
	private ListView mListView;
	private DataAdapter mAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.contact_list_with_add_layout, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView = (ListView) getView().findViewById(R.id.list_view);
		mAdapter = new DataAdapter(getActivity());
		mListView.setAdapter(mAdapter);
		
		getView().findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(), InputActivity.class), 0);
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case 0: // this is request code that is sent when calling startActivityForResult
				mAdapter.add(new Data(data.getStringExtra("name"), data.getStringExtra("description")));
				// after this should we call mAdapter.notifyDataSetChanged();
				// but because of we use ArrayAdapter, this will be auto called by the class
				break;
			}
		}
	}

}
