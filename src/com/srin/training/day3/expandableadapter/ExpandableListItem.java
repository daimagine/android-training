package com.srin.training.day3.expandableadapter;

import java.util.List;

public interface ExpandableListItem<T> {

	T getGroupItem();

	List<T> getChildItem();
}
