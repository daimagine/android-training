package com.srin.training.day3.expandableadapter;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.srin.training.R;

public class ExpandableListStringAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<ExpandableListItem<String>> expandableListItems;
	
	private View.OnClickListener onClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			if (v instanceof Button) {

				Toast.makeText(context, "Button Name : " + ((Button) v).getText(), Toast.LENGTH_SHORT).show();
			}
		}
	};

	public ExpandableListStringAdapter(Context context, List<ExpandableListItem<String>> expandableListItems) {

		this.context = context;
		this.expandableListItems = expandableListItems;
	}

	@Override
	public int getGroupCount() {

		return expandableListItems.size();
	}

	@Override
	public Object getGroup(int groupPosition) {

		return expandableListItems.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {

		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		GroupViewHolder holder;

		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.text_list_item, null);

			holder = new GroupViewHolder();
			holder.textView = (TextView) convertView.findViewById(R.id.text_view);
			holder.textView.setTypeface(null, Typeface.BOLD);
			convertView.setTag(holder);

		} else {

			holder = (GroupViewHolder) convertView.getTag();
		}

		holder.name = expandableListItems.get(groupPosition).getGroupItem();
		holder.textView.setText(holder.name);

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		return expandableListItems.get(groupPosition).getChildItem().size();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		return expandableListItems.get(groupPosition).getChildItem().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {

		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

		ChildViewHolder holder;

		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.button_list_item, null);

			holder = new ChildViewHolder();
			holder.button = (Button) convertView.findViewById(R.id.button);
			holder.button.setFocusable(false);
			holder.button.setOnClickListener(onClickListener);
			convertView.setTag(holder);

		} else {

			holder = (ChildViewHolder) convertView.getTag();
		}

		holder.name = expandableListItems.get(groupPosition).getChildItem().get(childPosition);
		holder.button.setText(holder.name);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {

		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {

		return true;
	}

	public static class GroupViewHolder {

		public TextView textView;
		public String name;
	}

	public static class ChildViewHolder {

		public Button button;
		public String name;
	}
}
