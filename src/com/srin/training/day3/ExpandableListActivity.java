package com.srin.training.day3;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.srin.training.R;
import com.srin.training.day3.expandableadapter.ExpandableListItem;
import com.srin.training.day3.expandableadapter.ExpandableListStringAdapter;
import com.srin.training.day3.expandableadapter.ExpandableListStringAdapter.ChildViewHolder;
import com.srin.training.day3.expandableadapter.ExpandableListStringAdapter.GroupViewHolder;

public class ExpandableListActivity extends Activity {

	private ExpandableListView expandableListView;
	private ExpandableListAdapter expandableListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		

		super.onCreate(savedInstanceState);
		setContentView(R.layout.day2_activity_expandable_list);
		
		final List<ExpandableListItem<String>> expandableListItems = generateDummyItems();
		
		expandableListAdapter = new ExpandableListStringAdapter(this, expandableListItems);

		expandableListView = (ExpandableListView) findViewById(R.id.list_view);
		expandableListView.setAdapter(expandableListAdapter);
		expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

				GroupViewHolder holder = (GroupViewHolder) v.getTag();
				Toast.makeText(ExpandableListActivity.this, "Group Name : " + holder.name, Toast.LENGTH_SHORT).show();

				return false;
			}
		});
		
		expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				
				ChildViewHolder holder = (ChildViewHolder) v.getTag();
				Toast.makeText(ExpandableListActivity.this, "Child Name : " + holder.name, Toast.LENGTH_SHORT).show();

				return true;
			}
		});
	}

	private List<ExpandableListItem<String>> generateDummyItems() {

		List<ExpandableListItem<String>> expandableListItems = new ArrayList<ExpandableListItem<String>>();

		ExpandableListItem<String> group1 = new ExpandableListItem<String>() {

			@Override
			public String getGroupItem() {

				return "Group 1";
			}

			@Override
			public List<String> getChildItem() {

				List<String> items = new ArrayList<String>();
				items.add("Child 1");
				items.add("Child 2");
				items.add("Child 3");

				return items;
			}
		};

		ExpandableListItem<String> group2 = new ExpandableListItem<String>() {

			@Override
			public String getGroupItem() {

				return "Group 2";
			}

			@Override
			public List<String> getChildItem() {

				List<String> items = new ArrayList<String>();
				items.add("Child 4");
				items.add("Child 5");
				items.add("Child 6");

				return items;
			}
		};

		expandableListItems.add(group1);
		expandableListItems.add(group2);
		
		return expandableListItems;
	}
}
