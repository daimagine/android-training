package com.srin.training.day8.actionbarwithappcompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.srin.training.R;
import com.srin.training.day8.SettingsActivity;

public class ActionBarExampleActivity extends ActionBarActivity {

	private Fragment mFragment;
	private SharedPreferences mSharedPreferences;
	private String mPrevTheme;
	private static final String FORCE_RESTART_ACTIVITY = "com.srin.training.day8.Extra.ForceRestartActivity";
	public static final int SETTINGS_REQUEST_CODE = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		String theme = mSharedPreferences.getString(
				SettingsActivity.PREF_KEY_THEME,
				getString(R.string.pref_theme_default_value));
		String userName = mSharedPreferences.getString(
				SettingsActivity.PREF_KEY_USER_NAME,
				getString(R.string.pref_user_name_default_value));
		// set theme based on preferences
		if (theme.equals("orange")) {
			setTheme(R.style.CustomAppTheme2AppCompat);
		} else {
			setTheme(R.style.CustomAppTheme2PurpleAppCompat);
		}
		mPrevTheme = theme;
		setContentView(R.layout.activity_master_shell_fragment);
		ActionBar actionBar = getSupportActionBar();
		// show home navigate up button in the action bar
		actionBar.setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState != null) {
			mFragment = getSupportFragmentManager().findFragmentById(
					R.id.fragment_container);
		}

		boolean showWelcomeMessage = !getIntent().getBooleanExtra(
				FORCE_RESTART_ACTIVITY, false);
		if (mFragment == null) {
			mFragment = new ContactListFragment();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.fragment_container, mFragment).commit();
		}
		if (showWelcomeMessage) {
			Toast.makeText(this, "Welcome, [" + userName + "]",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// add this line to make sure the navigate up will be triggered to
		// navigate to parent
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			break;
		case R.id.item_settings:
			startActivityForResult(new Intent(this, SettingsActivity.class),
					SETTINGS_REQUEST_CODE);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.day7_actionbar_menu, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == SETTINGS_REQUEST_CODE) {
			String theme = PreferenceManager.getDefaultSharedPreferences(this)
					.getString(SettingsActivity.PREF_KEY_THEME,
							getString(R.string.pref_theme_default_value));
			if (!theme.equals(mPrevTheme)) {
				restartActivityOnThemeChange(theme);
			}
		}
	}

	public void restartActivityOnThemeChange(String theme) {
		if (!theme.equals(mPrevTheme)) {
			// restart activity only if the theme is changed
			Intent intent = getIntent();
			intent.putExtra(FORCE_RESTART_ACTIVITY, true);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			// remove animation when restart activity
			overridePendingTransition(0, 0);
		}
	}
}
