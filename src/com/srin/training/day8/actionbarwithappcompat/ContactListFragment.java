package com.srin.training.day8.actionbarwithappcompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.srin.training.R;
import com.srin.training.day3.cursoradapter.ContactsFragment;
import com.srin.training.day8.SettingsActivity;
import com.srin.training.day8.actionbar.SettingsFragment;

public class ContactListFragment extends Fragment  implements LoaderCallbacks<Cursor> {
	

	//adapter n list view
	private ListView mListView;
	private CursorAdapter mCursorAdapter;
	private Toast mLastToast;
	
	//loader ID
	public static final int LOADER_ID_CONTACTS = 1;
	
	private static final String TAG = ContactListFragment.class.getSimpleName();

	private ProgressDialog mProgressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.contact_list_layout, container, false);
		// add this line to make sure that your fragment menu is created
		setHasOptionsMenu(true);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView = (ListView) getView().findViewById(R.id.list_view);
		mCursorAdapter = new ContactCursorAdapter(getActivity(), null, false);

		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		mListView.setAdapter(mCursorAdapter);
		getLoaderManager().initLoader(LOADER_ID_CONTACTS, null, this);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle arguments) {
		if(id == LOADER_ID_CONTACTS) {
			if (mProgressDialog != null) {
				mProgressDialog.show();
			}
			
			String[] projection = {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
			
			return new CursorLoader(getActivity(), ContactsContract.Contacts.CONTENT_URI, 
					projection, null, null, null);
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if(cursor != null && cursor.getCount() > 0) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mCursorAdapter.swapCursor(cursor);
		}
		
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mCursorAdapter.swapCursor(null);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.removeItem(R.id.item_search);
		
		inflater.inflate(R.menu.day7_actionbar_menu_fragment_list, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_info:
			if (mLastToast != null) {
				mLastToast.cancel();
			}
			mLastToast = Toast.makeText(getActivity(), "Show all contact name",
					Toast.LENGTH_SHORT);
			mLastToast.show();
			break;
			
		case R.id.item_refresh:
			getLoaderManager().restartLoader(LOADER_ID_CONTACTS, null, this);
			break;
			
		}
		return super.onOptionsItemSelected(item);
	}

}
