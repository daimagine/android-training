package com.srin.training.day8.actionbar;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

import com.srin.training.R;
import com.srin.training.day3.cursoradapter.ContactsFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ContactListFragment extends ContactsFragment implements
		OnQueryTextListener {

	private static final String TAG = ContactListFragment.class.getSimpleName();

	private Toast mLastToast;
	private SearchView mSearchView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		// add this line to make sure that your fragment menu is created
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.day7_actionbar_menu_fragment_list, menu);

		MenuItem menuItem = menu.findItem(R.id.item_search);
		mSearchView = (SearchView) menuItem.getActionView();
		mSearchView.setQueryHint("type something...");
		mSearchView.setOnQueryTextListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onOptionsItemSelected");
		switch (item.getItemId()) {
		case R.id.item_info:
			if (mLastToast != null) {
				mLastToast.cancel();
			}
			mLastToast = Toast.makeText(getActivity(), "Show all contact name",
					Toast.LENGTH_SHORT);
			mLastToast.show();
			break;

		case R.id.item_refresh:
			Bundle bundle = new Bundle();
			bundle.putBoolean(FORCE_PROGRESSBAR, true);
			getLoaderManager().restartLoader(LOADER_ID_CONTACTS, null, this);
			break;

		case R.id.item_search:
			Log.d(TAG, "onOptionsItemSelected item_search");
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private AsyncTask<Void, Void, Void> mTask;
	
	private Timer mTimer = new Timer();
	class QueryTimer extends TimerTask {
		private Bundle mBundle;
		public Bundle getBundle() {
			return mBundle;
		}
		public void setBundle(Bundle bundle) {
			this.mBundle = bundle;
		}
		@Override
		public void run() {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Log.d(TAG, "TimerTask. invoke loader");
					getLoaderManager().restartLoader(LOADER_ID_CONTACTS, mBundle,
							ContactListFragment.this);
				}
			});
		}
	}
	
	Bundle mQueryBundle;
	Handler mQueryHandler = new Handler();
	Runnable mQueryRunnable = new Runnable() {
		@Override
		public void run() {
			Log.d(TAG, "TimerTask. invoke loader");
			getLoaderManager().restartLoader(LOADER_ID_CONTACTS, mQueryBundle,
					ContactListFragment.this);
		}
	};

	@Override
	public boolean onQueryTextChange(String newText) {
		Log.d(TAG, "onQueryTextChange");
		final Bundle bundle = new Bundle();
		if (newText.length() > 0) {
			bundle.putString(CRITERIA_NAME, newText);
		}
		
		//using handler
//		mQueryBundle = bundle;
//		mQueryHandler.postDelayed(mQueryRunnable, 350);
		
		//using timer
//		mTimer.cancel();
//		mTimer.purge();
//		
//		QueryTimer queryTask = new QueryTimer();
//		queryTask.setBundle(bundle);
//		mTimer = new Timer();
//		mTimer.schedule(queryTask, 0, 500);		

		//using asynctask : working smooth
		if (mTask != null) {
			mTask.cancel(true);
		}
		mTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				try {
					Thread.sleep(350);
				} catch (InterruptedException exception) {
					return null;
				}
				return null;
			}

			protected void onPostExecute(Void result) {
				Log.d(TAG, "Thread. invoke loader");
				getLoaderManager().restartLoader(LOADER_ID_CONTACTS, bundle,
						ContactListFragment.this);
			}
		};
		mTask.execute();
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

}
