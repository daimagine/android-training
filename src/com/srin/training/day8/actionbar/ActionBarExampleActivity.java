package com.srin.training.day8.actionbar;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.srin.training.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ActionBarExampleActivity extends Activity {

	private Fragment mFragment;
	private SharedPreferences mSharedPreferences;
	private String mPrevTheme;
	private static final String FORCE_FRAGMENT_EXTRA = "com.srin.training.day8.Extra.ForceFragment";
	private static final String EXTRA_FRAGMENT_SETTINGS = "settings";

	private static final String TAG = ActionBarExampleActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		
		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		String theme = mSharedPreferences.getString(
				SettingsFragment.PREF_KEY_THEME,
				getString(R.string.pref_theme_default_value));
		String userName = mSharedPreferences.getString(
				SettingsFragment.PREF_KEY_USER_NAME,
				getString(R.string.pref_user_name_default_value));
		// set theme based on preferences
		if (theme.equals("orange")) {
			setTheme(R.style.CustomAppTheme2);
		} else {
			setTheme(R.style.CustomAppTheme2Purple);
		}
		mPrevTheme = theme;
		setContentView(R.layout.activity_master_shell_fragment);
		ActionBar actionBar = getActionBar();
		// show home navigate up button in the action bar
		actionBar.setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState != null) {
			mFragment = getFragmentManager().findFragmentById(
					R.id.fragment_container);
		}

		boolean showWelcomeMessage = true;
		if (mFragment == null) {
			String extraFragment = getIntent().getStringExtra(
					FORCE_FRAGMENT_EXTRA);
			mFragment = new ContactListFragment();
			getFragmentManager().beginTransaction()
					.replace(R.id.fragment_container, mFragment).commit();
			// push settings fragment if this activity started from restart on
			// theme change
			if (EXTRA_FRAGMENT_SETTINGS.equals(extraFragment)) {
				mFragment = new SettingsFragment();
				getFragmentManager().beginTransaction()
						.replace(R.id.fragment_container, mFragment)
						.addToBackStack(null).commit();
				showWelcomeMessage = false;
			}
		}
		if (showWelcomeMessage) {
			Toast.makeText(this, "Welcome, [" + userName + "]",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// add this line to make sure the navigate up will be triggered to
		// navigate to parent
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			break;
		case R.id.item_settings:
			if (!(getFragmentManager()
					.findFragmentById(R.id.fragment_container) instanceof SettingsFragment)) {
				mFragment = new SettingsFragment();
				getFragmentManager().beginTransaction()
						.replace(R.id.fragment_container, mFragment)
						.addToBackStack(null).commit();
			}
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.day7_actionbar_menu, menu);
		return true;
	}

	public void restartActivityOnThemeChange(String theme) {
		if (!theme.equals(mPrevTheme)) {
			// restart activity only if the theme is changed
			Intent intent = getIntent();
			intent.putExtra(FORCE_FRAGMENT_EXTRA, EXTRA_FRAGMENT_SETTINGS);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			// remove animation when restart activity
			overridePendingTransition(0, 0);
		}
	}
}
