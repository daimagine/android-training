package com.srin.training.day8.actionbar;

import com.srin.training.R;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SettingsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {

	public static final String PREF_KEY_THEME = "pref_theme";
	public static final String PREF_KEY_USER_NAME = "pref_user_name";
	public static final String PREF_KEY_ABOUT = "pref_about";
	private SharedPreferences mPreferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Load preference from XML resource
		addPreferencesFromResource(R.xml.app_preference);
		mPreferences = getPreferenceManager().getSharedPreferences();
		// force set summary for each fragment
		onSharedPreferenceChanged(mPreferences, PREF_KEY_THEME);
		onSharedPreferenceChanged(mPreferences, PREF_KEY_USER_NAME);

		findPreference(PREF_KEY_ABOUT).setOnPreferenceClickListener(
				new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						new AlertDialog.Builder(getActivity())
								.setTitle(getString(R.string.app_name))
								.setMessage(
										"This is a sample of application about dialog.")
								.setPositiveButton("Ok", null).show();
						return false;
					}
				});
	}

	@Override
	public void onStart() {
		super.onStart();
		mPreferences.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		mPreferences.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(PREF_KEY_THEME) || key.equals(PREF_KEY_USER_NAME)) {
			String defValue = key.equals(PREF_KEY_THEME) ? getString(R.string.pref_theme_default_value)
					: getString(R.string.pref_user_name_default_value);
			String value = sharedPreferences.getString(key, defValue);
			findPreference(key).setSummary(value);
			if (key.equals(PREF_KEY_THEME)
					&& getActivity() instanceof ActionBarExampleActivity) {
				((ActionBarExampleActivity) getActivity())
						.restartActivityOnThemeChange(value);
			}
		}
	}

}
