package com.srin.training.day2;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.srin.training.R;

public class MasterFragment extends Fragment {
	
	public static final String LAYOUT_NAME_LIST = "layout_array";
	
	private OnLayoutSelectedListener onLayoutSelectedListener;
	
	@Override
	public void onAttach(Activity activity) {

		Log.d("TRAINING", "MasterFragment.onAttach");
		super.onAttach(activity);
		
		if (activity instanceof OnLayoutSelectedListener) {
			
			onLayoutSelectedListener = (OnLayoutSelectedListener) activity;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		Log.d("TRAINING", "MasterFragment.onCreate");
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Log.d("TRAINING", "MasterFragment.onCreateView");
		return inflater.inflate(R.layout.day2_fragment_master, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		Log.d("TRAINING", "MasterFragment.onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		
		ListView listView = (ListView) getView().findViewById(R.id.list_view);
		
		final String[] layouts = getArguments().getStringArray(LAYOUT_NAME_LIST);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, layouts);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				onLayoutSelectedListener.onLayoutSelected(layouts[position]);
			}
		});
	}
	
	@Override
	public void onStart() {

		Log.d("TRAINING", "MasterFragment.onStart");
		super.onStart();
	}
	
	@Override
	public void onPause() {

		Log.d("TRAINING", "MasterFragment.onPause");
		super.onPause();
	}
	
	@Override
	public void onStop() {

		Log.d("TRAINING", "MasterFragment.onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroyView() {

		Log.d("TRAINING", "MasterFragment.onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy() {

		Log.d("TRAINING", "MasterFragment.onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onDetach() {

		Log.d("TRAINING", "MasterFragment.onDetach");
		super.onDetach();

		onLayoutSelectedListener = null;
	}
	
	public interface OnLayoutSelectedListener {
		
		void onLayoutSelected(String layoutName);
	}
}
