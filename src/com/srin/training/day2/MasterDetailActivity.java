package com.srin.training.day2;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.srin.training.R;

public class MasterDetailActivity extends Activity implements MasterFragment.OnLayoutSelectedListener {
	
	private String layoutName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.d("TRAINING", "MasterDetailActivity.onCreate");
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null) {
			
			if (savedInstanceState.containsKey(DetailFragment.LAYOUT_NAME)) {
				
				layoutName = savedInstanceState.getString(DetailFragment.LAYOUT_NAME);
			}
		}
		
		init();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		
		Log.d("TRAINING", "MasterDetailActivity.onConfigurationChanged");
		
		super.onConfigurationChanged(newConfig);
		
		init();
	}
	
	private void init() {
		
		setContentView(R.layout.activity_master_detail);
		
		if (findViewById(R.id.master_layout) != null) {
			
			Bundle bundle = getIntent().getExtras();
			
			if (bundle == null) {
				bundle = new Bundle();
			}
			
			bundle.putStringArray(MasterFragment.LAYOUT_NAME_LIST, new String[] {"Linear Vertical", "Linear Horizontal", "Relative", "Frame"});
			
			MasterFragment masterFragment = new MasterFragment();
			masterFragment.setArguments(bundle);
			
			getFragmentManager().beginTransaction().add(R.id.master_layout, masterFragment).commit();
		}
		
		if (findViewById(R.id.detail_layout) != null && layoutName != null) {
			
			onLayoutSelected(layoutName);
		}
	}

	@Override
	protected void onStart() {

		Log.d("TRAINING", "MasterDetailActivity.onStart");
		super.onStart();
	}
	
	@Override
	protected void onResume() {

		Log.d("TRAINING", "MasterDetailActivity.onResume");
		super.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		
		outState.putString(DetailFragment.LAYOUT_NAME, layoutName);
	}
	
	@Override
	protected void onPause() {

		Log.d("TRAINING", "MasterDetailActivity.onPause");
		super.onPause();
	}
	
	@Override
	protected void onStop() {

		Log.d("TRAINING", "MasterDetailActivity.onStop");
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {

		Log.d("TRAINING", "MasterDetailActivity.onDestroy");
		super.onDestroy();
	}

	@Override
	public void onLayoutSelected(String layoutName) {
		
		Log.d("TRAINING", "MasterDetailActivity.onLayoutSelected.layoutName : " + layoutName);
		
		this.layoutName = layoutName;
			
		Bundle bundle = getIntent().getExtras();
		
		if (bundle == null) {
			bundle = new Bundle();
		}
		
		bundle.putString(DetailFragment.LAYOUT_NAME, layoutName);
		
		DetailFragment detailFragment = new DetailFragment();
		detailFragment.setArguments(bundle);
		
		FragmentManager manager = getFragmentManager();
		
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		
		if (findViewById(R.id.detail_layout) != null) {
			
			Log.d("TRAINING", "MasterDetailActivity.onLayoutSelected.dualPane");
			transaction.replace(R.id.detail_layout, detailFragment);
			
		} else {
			
			transaction.replace(R.id.master_layout, detailFragment);
		}
		
		transaction.addToBackStack(null);
		transaction.commit();
	}
}
