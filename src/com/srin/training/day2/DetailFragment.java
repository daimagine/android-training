package com.srin.training.day2;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.srin.training.R;

public class DetailFragment extends Fragment {

	public static final String LAYOUT_NAME = "layout_name";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		Log.d("TRAINING", "DetailFragment.onCreateView");
		
		View view = null;
		
		String layoutName = getArguments().getString(LAYOUT_NAME);
		
		if ("Linear Vertical".equals(layoutName)) {
			
			view = inflater.inflate(R.layout.day2_fragment_detail_linear_vertical, container, false);
			
		} else if ("Linear Horizontal".equals(layoutName)) {
			
			view = inflater.inflate(R.layout.day2_fragment_detail_linear_horizontal, container, false);
			
		} else if ("Relative".equals(layoutName)) {
			
			view = inflater.inflate(R.layout.day2_fragment_detail_relative, container, false);
			
		} else {
			
			view = inflater.inflate(R.layout.day2_fragment_detail_frame, container, false);
		}
		
		return view;
	}
}
