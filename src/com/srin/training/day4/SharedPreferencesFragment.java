package com.srin.training.day4;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.srin.training.R;

public class SharedPreferencesFragment extends Fragment {
	
	public static final String PREFS_NAME = "com.srin.training";
	public static final String PREF_WIFI_STATE = "com.srin.training.WIFI_STATE";
	public static final String PREF_BLUETOOTH_STATE = "com.srin.training.BLUETOOTH_STATE";
	
	private Activity activity;

	private CheckBox wifiCheck;
	private CheckBox bluetoothCheck;
	
	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		this.activity = activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.day4_fragment_shared_preferences, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		
		wifiCheck = (CheckBox) getView().findViewById(R.id.wifi_check);
		wifiCheck.setChecked(readPreference(PREF_WIFI_STATE, Boolean.class));
		wifiCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				writePreference(PREF_WIFI_STATE, isChecked);
			}
		});
		
		bluetoothCheck = (CheckBox) getView().findViewById(R.id.bluetooth_check);
		bluetoothCheck.setChecked((readPreference(PREF_BLUETOOTH_STATE, Boolean.class) ? true : false));
		bluetoothCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				writePreference(PREF_BLUETOOTH_STATE, (isChecked ? 1 : 0));
			}
		});
	}
	
	@Override
	public void onDestroy() {

		super.onDestroy();
		
		activity = null;
	}
	
	private <T> T readPreference(String key, Class<T> type) {
		
		SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		Log.d("TRAINING", "readPreference." + key + " : " + settings.contains(key));
		
		if (type.equals(Boolean.class)) {
			
			return type.cast(settings.getBoolean(key, false));
		}
		
		if (type.equals(Integer.class)) {
			
			return type.cast(settings.getInt(key, 0));
		}
		
		return null;
	}
	
	private void writePreference(String key, boolean val) {
		
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, val);
		editor.commit();
		
		Log.d("TRAINING", "writePreference." + key + " : " + settings.contains(key));
	}
	
	private void writePreference(String key, int val) {
		
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, val);
		editor.commit();
		
		Log.d("TRAINING", "writePreference." + key + " : " + settings.contains(key));
	}
}
