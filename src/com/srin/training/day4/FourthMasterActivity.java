package com.srin.training.day4;

import java.util.HashMap;

import com.srin.training.R;
import com.srin.training.day3.cursoradapter.ContactsFragment;
import com.srin.training.day4.async.ImageAsyncTaskFragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

public class FourthMasterActivity extends Activity implements
		MasterMenuFragment.TopicSelectedListener {

	private static final String TAG = "SRIN FourthMasterActivity";
	private static final String KEY_LAST_TOPIC = "key_last_topic";
	
	private int mLastTopic;
	private boolean mDualPane;
	
	private static final int MASTER_MENU_FRAGMENT = 0;
	private static final int MENU_ASYNCTASK = 1;
	
	@SuppressLint("UseSparseArrays")
	private static HashMap<Integer, String> MENUS = new HashMap<Integer, String>();
	
	static {
		MENUS.put(MENU_ASYNCTASK, "AsyncTask");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "FourMasterActivity.onCreate");
		setContentView(R.layout.activity_master_detail);
		mDualPane = findViewById(R.id.detail_layout) != null;
		if (savedInstanceState != null) {
			mLastTopic = savedInstanceState.getInt(KEY_LAST_TOPIC, MASTER_MENU_FRAGMENT);
		
			Log.d(TAG, "FourMasterActivity mLastTopic : " + mLastTopic);
			if (mLastTopic != MASTER_MENU_FRAGMENT && mDualPane) {
				onTopicSelected(MASTER_MENU_FRAGMENT);
			}
		} else {
			mLastTopic = MASTER_MENU_FRAGMENT;
		}
		onTopicSelected(mLastTopic);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_LAST_TOPIC, mLastTopic);
	}

	@Override
	public void onTopicSelected(int topic) {
		Log.d(TAG, "FourthMasterActivity.onTopicSelected topicName : " + topic);
		int layoutId = mDualPane ? R.id.detail_layout : R.id.master_layout;
		Fragment fragment = null;
		switch (topic) {
		case MASTER_MENU_FRAGMENT:
			Bundle bundle = new Bundle();
			bundle.putSerializable(MasterMenuFragment.MENUS_KEY, MENUS);
			fragment = new MasterMenuFragment();
			fragment.setArguments(bundle);
			layoutId = R.id.master_layout;
			break;
			
		case MENU_ASYNCTASK:
			fragment = new ImageAsyncTaskFragment();
			break;

		default:
			break;
		}
		
		if (fragment == null) {
			throw new RuntimeException("Invalid Topic Selected");
		}
		
		getFragmentManager().beginTransaction()
				.replace(layoutId, fragment)
				.commit();
		
		if (topic != MASTER_MENU_FRAGMENT) {
			mLastTopic = topic;
		}
	}

}
