package com.srin.training.day4;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.srin.training.R;

public class DialogExampleListFragment extends Fragment implements
		OnItemClickListener {

	private ListView mListView;
	private OnListItemClickListener mListItemClickListener;

	private static String[] MENUS = { "Alert Dialog", "Progress Dialog" , "Dialog Fragment", "Date Picker Dialog" };

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnListItemClickListener) {
			mListItemClickListener = (OnListItemClickListener) activity;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListItemClickListener = null;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_master, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView = (ListView) getView().findViewById(R.id.list_view);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, MENUS);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long itemId) {
		if (mListItemClickListener != null) {
			mListItemClickListener.onListItemClick(MENUS[position]);
		}
	}

	public static interface OnListItemClickListener {
		public void onListItemClick(String menu);
	}

}
