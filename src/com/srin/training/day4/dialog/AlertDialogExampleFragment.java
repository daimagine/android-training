package com.srin.training.day4.dialog;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.srin.training.R;

public class AlertDialogExampleFragment extends Fragment {

	private static final String[] ITEMS = { "One", "Two", "Three", "Four",
			"Five" };
	private EditText mEditTextTitle;
	private EditText mEditTextMessage;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.day4_fragment_alert_dialog, container,
				false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mEditTextTitle = (EditText) getView()
				.findViewById(R.id.edit_text_title);
		mEditTextMessage = (EditText) getView().findViewById(
				R.id.edit_text_message);
		// simple alert dialog
		getView().findViewById(R.id.button_show_alert).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						new AlertDialog.Builder(getActivity())
								.setTitle(mEditTextTitle.getText().toString())
								.setMessage(
										mEditTextMessage.getText().toString())
								.setOnCancelListener(
										new DialogInterface.OnCancelListener() {

											@Override
											public void onCancel(
													DialogInterface dialog) {
												Toast.makeText(getActivity(),
														"Dialog canceled",
														Toast.LENGTH_SHORT)
														.show();
											}
										})
								.setCancelable(true)
								.setNegativeButton("Cancel",
										new OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												Toast.makeText(
														getActivity(),
														"Dialog negative button clicked",
														Toast.LENGTH_SHORT)
														.show();
											}
										})
								.setNeutralButton("Maybe",
										new OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												Toast.makeText(
														getActivity(),
														"Dialog neutral button clicked",
														Toast.LENGTH_SHORT)
														.show();
											}
										})
								.setPositiveButton("Ok", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Toast.makeText(
												getActivity(),
												"Dialog positive button clicked",
												Toast.LENGTH_SHORT).show();
									}
								}).show();
					}
				});

		// alert dialog with list
		getView().findViewById(R.id.button_show_list_alert).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						new AlertDialog.Builder(getActivity())
								.setTitle("Choose one")
								.setItems(ITEMS, new OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Toast.makeText(getActivity(),
												"Selected: " + ITEMS[which],
												Toast.LENGTH_SHORT).show();
									}
								}).show();
					}

				});

		// alert dialog with single choice list
		getView().findViewById(R.id.button_show_choice_alert)
				.setOnClickListener(new View.OnClickListener() {

					private int mSelected;

					@Override
					public void onClick(View v) {
						new AlertDialog.Builder(getActivity())
								.setTitle("Choose one")
								.setSingleChoiceItems(ITEMS, 0,
										new OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												Toast.makeText(
														getActivity(),
														"Selected: "
																+ ITEMS[which],
														Toast.LENGTH_SHORT)
														.show();
												mSelected = which;
											}
										})
								.setPositiveButton("Ok", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Toast.makeText(
												getActivity(),
												"Dialog positive button clicked with selected: "
														+ ITEMS[mSelected],
												Toast.LENGTH_SHORT).show();
									}
								}).show();
					}
				});

		// alert dialog with multiple choice list
		getView().findViewById(R.id.button_show_multi_choice_alert)
				.setOnClickListener(new View.OnClickListener() {

					private ArrayList<String> mSelectedItems = new ArrayList<String>();

					@Override
					public void onClick(View v) {
						new AlertDialog.Builder(getActivity())
								.setTitle("Choose items")
								.setMultiChoiceItems(ITEMS, null,
										new OnMultiChoiceClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which, boolean isChecked) {
												if (isChecked) {
													mSelectedItems
															.add(ITEMS[which]);
													Toast.makeText(
															getActivity(),
															"Checked: "
																	+ ITEMS[which],
															Toast.LENGTH_SHORT)
															.show();
												} else {
													mSelectedItems
															.remove(ITEMS[which]);
													Toast.makeText(
															getActivity(),
															"Unchecked: "
																	+ ITEMS[which],
															Toast.LENGTH_SHORT)
															.show();
												}
											}
										})
								.setPositiveButton("Ok", new OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Toast.makeText(
												getActivity(),
												"Dialog positive button clicked with selected: "
														+ mSelectedItems
																.toString(),
												Toast.LENGTH_SHORT).show();
									}
								}).show();
					}
				});

		// alert dialog with custom layout
		getView().findViewById(R.id.button_show_custom_alert)
				.setOnClickListener(new View.OnClickListener() {

					private AlertDialog dialog;
					
					@Override
					public void onClick(View v) {
						View contentView = LayoutInflater
								.from(getActivity())
								.inflate(
										R.layout.day4_layout_custom_alert_dialog,
										null);
						// set event to custom view
						contentView.findViewById(R.id.button_dialog_custom)
								.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View v) {
										Toast.makeText(getActivity(),
												"Custom dialog button clicked",
												Toast.LENGTH_SHORT).show();
										// because of this is a custom view, this button will not auto dismiss the dialog
										dialog.dismiss();
									}
								});
						dialog = new AlertDialog.Builder(getActivity()).setView(
								contentView).show();
					}
				});
	}

}
