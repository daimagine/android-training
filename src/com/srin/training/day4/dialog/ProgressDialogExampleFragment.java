package com.srin.training.day4.dialog;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.srin.training.R;

public class ProgressDialogExampleFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.day4_fragment_progress_dialog,
				container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// indeterminate loading
		getView().findViewById(R.id.button_show_indeterminate)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ProgressDialog.show(getActivity(), "Loading",
								"Indeterminate Loading", true, true,
								new OnCancelListener() {

									@Override
									public void onCancel(DialogInterface dialog) {
										Toast.makeText(getActivity(),
												"Loading cancelled",
												Toast.LENGTH_SHORT).show();
									}
								});
					}
				});
		// determinate loading
		getView().findViewById(R.id.button_show_determinate)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ProgressDialog progressDialog = new ProgressDialog(
								getActivity());
						progressDialog.setCancelable(true);
						progressDialog.setTitle("Loading");
						progressDialog.setMessage("60%");
						progressDialog.setIndeterminate(false);
						progressDialog.setMax(100);
						progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
						progressDialog
								.setOnCancelListener(new OnCancelListener() {

									@Override
									public void onCancel(DialogInterface dialog) {
										Toast.makeText(getActivity(),
												"Loading cancelled",
												Toast.LENGTH_SHORT).show();
									}
								});
						progressDialog.show();
						progressDialog.setProgress(60);
					}
				});
	}

}
