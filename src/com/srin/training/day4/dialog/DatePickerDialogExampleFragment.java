package com.srin.training.day4.dialog;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.srin.training.R;

public class DatePickerDialogExampleFragment extends Fragment {

	private TextView mDateText;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.day4_fragment_datepicker_dialog,
				container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mDateText = (TextView) getView().findViewById(R.id.textview_date);
		getView().findViewById(R.id.button_show_datepicker).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DatePickerDialog dialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						mDateText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
					}
				}, 1970, 0, 1);
				dialog.show();
			}
		});
	}

}
