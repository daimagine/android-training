package com.srin.training.day4.async;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

public class ImageAsyncTask extends AsyncTask<String, Integer, String> {

	private static final String TAG = "SRIN ImageAsyncTask";
	private final WeakReference<ImageView> imageViewReference;
	private ProgressDialog pDialog;
	
	private Object LOCK = new Object();
	private String mImagePath;

	public ImageAsyncTask(ImageView imageView, ProgressDialog dialog) {
		imageViewReference = new WeakReference<ImageView>(imageView);
		pDialog = dialog;
	}
	
	public String getImagePath() {
		synchronized (LOCK) {
			return mImagePath;
		}
	}

	private void setImagePath(String path) {
		synchronized (LOCK) {
			mImagePath = path; 
		}
	}
	
	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecuted");
		super.onPreExecute();
		pDialog.show();
	}

	@Override
	protected String doInBackground(String... params) {
		String urlImage = params[0];
		URL url;
		int count;
		String imageTemp = null;
		Log.d(TAG, "doInBackground. url : " + urlImage);

		try {

			url = new URL(urlImage);
			URLConnection conection = url.openConnection();
			conection.connect();
			// getting file length
			int lengthOfFile = conection.getContentLength();
			
			Log.d(TAG, "doInBackground. length of file : " + lengthOfFile);

			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			// Output stream
			imageTemp = Environment.getExternalStorageDirectory().toString()
					+ "/downloadedfile.jpg";
			OutputStream output = new FileOutputStream(imageTemp);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				publishProgress((int) ((total * 100) / lengthOfFile));
				
				Thread.sleep(1000);

				// writing data to file
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();
			
			setImagePath(imageTemp);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageTemp;
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		// setting progress percentage
		Log.d(TAG, "onProgressUpdate. progress : " + progress[0]);
		pDialog.setProgress(progress[0]);
	}

	@Override
	protected void onCancelled() {
		Log.d(TAG, "onCancelled");
		super.onCancelled();
		if (pDialog != null) {
			pDialog.dismiss();
		}
		pDialog = null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		Log.d(TAG, "onPostExecute");
		pDialog.dismiss();
		
		String imagePath = result;
		
		if (imageViewReference != null && imagePath != null) {
			final ImageView imageView = imageViewReference.get();
			if (imageView != null) {
				Log.d(TAG, "onPostExecute. imageView.setImageBitmap");

				// Displaying downloaded image into image view
				imageView.setImageDrawable(Drawable.createFromPath(imagePath));
			}
		}
	}
}
