package com.srin.training.day4.async;

import com.srin.training.R;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class ImageAsyncTaskFragment extends Fragment {

	private static final String TAG = "SRIN ImageAsyncTaskFragment";
	
	private ImageView mImageAvatar;
	private Button mImageButton;
	private static final String AVATAR_URL = "http://www.gravatar.com/avatar/0";
	private static final String AVATAR_IMG = "avatar_image_path_key";
	private String mImageAvatarPath;
	private ImageAsyncTask mImageLoader;
	
	private ProgressDialog pDialog;
	
	public ImageAsyncTaskFragment() {}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.image_async_layout, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null && 
				savedInstanceState.getString(AVATAR_IMG) != null) {
			mImageAvatarPath = savedInstanceState.getString(AVATAR_IMG);
			Log.d(TAG, "mImageAvatarPath : " + mImageAvatarPath);
		}
		
		mImageAvatar = (ImageView) getView().findViewById(R.id.avatar_image);
		mImageButton = (Button) getView().findViewById(R.id.load_button);
		
		pDialog = new ProgressDialog(getActivity());
		pDialog.setCancelable(false);
        pDialog.setMessage("Downloading ...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
		
		mImageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "execute asynctask");
				mImageLoader = new ImageAsyncTask(mImageAvatar, pDialog);
				mImageLoader.execute(AVATAR_URL);
			}
		});
	}
	
	@Override
	public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		if (mImageAvatarPath != null && mImageAvatar != null) {
			Log.d(TAG, "draw image avatar " + mImageAvatarPath);
			mImageAvatar.setImageDrawable(
					Drawable.createFromPath(mImageAvatarPath));
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		super.onSaveInstanceState(outState);
		if (mImageLoader != null && mImageLoader != null) {
			outState.putString(AVATAR_IMG, mImageLoader.getImagePath());
		}
	}
	
	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		if (pDialog != null) {
			pDialog.dismiss();
		}
		pDialog = null;
		mImageAvatarPath = null;
	}
	
}
