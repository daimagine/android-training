package com.srin.training.day4;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.widget.Toast;

import com.srin.training.R;

public class SharedPreferencesActivity extends Activity implements OnSharedPreferenceChangeListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.day4_activity_shared_preferences);
		
		getSharedPreferences(SharedPreferencesFragment.PREFS_NAME, Context.MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
		
		if (findViewById(R.id.master_layout) != null) {
			
			Bundle bundle = getIntent().getExtras();
			
			if (bundle == null) {
				bundle = new Bundle();
			}
			
			SharedPreferencesFragment fragment = new SharedPreferencesFragment();
			fragment.setArguments(bundle);
			
			getFragmentManager().beginTransaction().add(R.id.master_layout, fragment).commit();
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		
		Toast.makeText(this, "shared preference " + key + " has been changed.", Toast.LENGTH_SHORT).show();
	}
}
