package com.srin.training.day4;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.srin.training.R;
import com.srin.training.day4.DialogExampleListFragment.OnListItemClickListener;
import com.srin.training.day4.dialog.AlertDialogExampleFragment;
import com.srin.training.day4.dialog.DatePickerDialogExampleFragment;
import com.srin.training.day4.dialog.FragmentDialogExampleFragment;
import com.srin.training.day4.dialog.ProgressDialogExampleFragment;

public class DialogExampleActivity extends Activity implements
		OnListItemClickListener {

	private static final String KEY_LAST_FRAGMENT = "key_last_fragment";

	private static final int LIST_FRAGMENT = 0;
	private static final int ALERT_DIALOG_FRAGMENT = 1;
	private static final int PROGRESS_DIALOG_FRAGMENT = 2;
	private static final int FRAGMENT_DIALOG_FRAGMENT = 3;
	private static final int DATEPICKER_DIALOG_FRAGMENT = 4;

	private int mLastFragment;
	private boolean mInLandscapeMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_master_detail);
		mInLandscapeMode = findViewById(R.id.detail_layout) != null;
		if (savedInstanceState != null) {
			mLastFragment = savedInstanceState.getInt(KEY_LAST_FRAGMENT,
					LIST_FRAGMENT);
			if (mLastFragment != LIST_FRAGMENT && mInLandscapeMode) {
				showFragment(LIST_FRAGMENT);
			}
		} else {
			mLastFragment = LIST_FRAGMENT;
		}
		showFragment(mLastFragment);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_LAST_FRAGMENT, mLastFragment);
	}

	private void showFragment(int whichFragment) {
		int layoutId = mInLandscapeMode ? R.id.detail_layout
				: R.id.master_layout;
		switch (whichFragment) {
		case LIST_FRAGMENT: {
			DialogExampleListFragment fragment = new DialogExampleListFragment();
			getFragmentManager().beginTransaction()
					.replace(R.id.master_layout, fragment).commit();
		}
			break;
		case ALERT_DIALOG_FRAGMENT: {
			AlertDialogExampleFragment fragment = new AlertDialogExampleFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		case PROGRESS_DIALOG_FRAGMENT: {
			ProgressDialogExampleFragment fragment = new ProgressDialogExampleFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		case FRAGMENT_DIALOG_FRAGMENT: {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment prev = getFragmentManager().findFragmentByTag("dialog");
			if (prev != null) {
				ft.remove(prev);
			}
			FragmentDialogExampleFragment fragment = new FragmentDialogExampleFragment();
			fragment.show(ft, "dialog");
		}
			break;
		case DATEPICKER_DIALOG_FRAGMENT: {
			DatePickerDialogExampleFragment fragment = new DatePickerDialogExampleFragment();
			getFragmentManager().beginTransaction().replace(layoutId, fragment)
					.commit();
		}
			break;
		}
		if (whichFragment != LIST_FRAGMENT) {
			mLastFragment = whichFragment;
		}
	}

	@Override
	public void onListItemClick(String menu) {
		if (menu.equals("Alert Dialog")) {
			showFragment(ALERT_DIALOG_FRAGMENT);
		} else if (menu.equals("Progress Dialog")) {
			showFragment(PROGRESS_DIALOG_FRAGMENT);
		} else if (menu.equals("Dialog Fragment")) {
			showFragment(FRAGMENT_DIALOG_FRAGMENT);
		} else if (menu.equals("Date Picker Dialog")) {
			showFragment(DATEPICKER_DIALOG_FRAGMENT);
		}
	}
}
