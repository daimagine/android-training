package com.srin.training.day4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.srin.training.R;

public class MasterMenuFragment extends Fragment implements OnItemClickListener {

	private static final String TAG = "SRIN MasterMenuFragment";
	
	private ListView mListView;
	private TopicSelectedListener mTopicSelectedListener;
	private HashMap<Integer, String> menus;
	private ArrayList<String> menuArray;
	public static final String MENUS_KEY = "master_menu_fragment_menu_key";
	
	public MasterMenuFragment() {}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, toString() + "MasterMenuFragment.onCreate");
		super.onCreate(savedInstanceState);
		this.menus = (HashMap<Integer, String>) getArguments().getSerializable(MENUS_KEY);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.d(TAG, toString() + "MasterMenuFragment.onAttach");
		if (activity instanceof TopicSelectedListener) {
			this.mTopicSelectedListener = (TopicSelectedListener) activity;
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		Log.d(TAG, toString() + "MasterMenuFragment.onDetach");
		mTopicSelectedListener = null;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, toString() + "MasterMenuFragment.onCreateView");
		return inflater
				.inflate(R.layout.fragment_master, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, toString() + "MasterMenuFragment.onActivityCreated");
		mListView = (ListView) getView().findViewById(R.id.list_view);
		
		menuArray = new ArrayList<String>(menus.values());
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), 
					android.R.layout.simple_list_item_1, 
					menuArray);
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long itemId) {
		if (mTopicSelectedListener != null) {
			String selectedTopic = menuArray.get(position);
			
			Log.d(TAG, "selectedTopic : " + selectedTopic);
			if (menus.containsValue(selectedTopic)) {
				for (Map.Entry<Integer, String> e : menus.entrySet()) {
					String value = e.getValue();
				    Integer key = e.getKey();
				    if (value.equals(selectedTopic)) {
				    	mTopicSelectedListener.onTopicSelected(key);
				    }
				}
			}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, toString() + "MasterMenuFragment.onDestroy");
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Log.d(TAG, toString() + "MasterMenuFragment.onDestroyView");
	}

	public static interface TopicSelectedListener {
		public void onTopicSelected(int topic);
	}
}
